This is an app designed to analyze outgoing http requests stemming from the windows OS it resides on separating login requests, capture entered credentials and store them in a text file.

Prerequisites: 
- install npcap: https://nmap.org/npcap/

To install as a windows service (post deployment):
1. run  `whatisprivacyeven install` on the installation folder
2. start the service through windows services
3. enjoy http credentials stored on the StoredRequests.txt at the installation folder

Note: Main project may be found in the RequestAnalyser folder

**Important note**: This is only meant as an experimental attack tool fueling technical research and any misusage shall fall upon the user's own shoulders.
