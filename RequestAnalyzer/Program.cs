﻿using System;
using System.IO;
using ConsoleApp1;
using PacketDotNet;
using PacketDotNet.Connections;
using PacketDotNet.Connections.Http;
using SharpPcap;
using Topshelf;

// Configure log4net using the .config file
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
// This will cause log4net to look for a configuration file
// called TestApp.exe.config in the application base
// directory (i.e. the directory containing TestApp.exe)
// The config file will be watched for changes.

namespace RequestAnalyzer
{
    class Program
    {
        public static void Main(string[] args)
        {
 
                HostFactory.Run
                    (serviceConfig =>
                    {
                        serviceConfig.Service<ReqAnalyserService>(serviceInstance =>
                        {
                            serviceInstance.ConstructUsing(
                                () => new ReqAnalyserService());
                            serviceInstance.WhenStarted(execute => execute.Start());
                            serviceInstance.WhenStopped(execute => execute.Stop());
                        });

                        serviceConfig.SetServiceName("RequestAnalyzer");
                        serviceConfig.SetDisplayName("Sneaky mf");
                        serviceConfig.SetDescription("None of your business mate");
                        serviceConfig.StartAutomatically();

                    });

                Console.Read();
        }
    }
}


