﻿using ConsoleApp1.Helpers;
using PacketDotNet;
using PacketDotNet.Connections;
using PacketDotNet.Connections.Http;
using SharpPcap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace RequestAnalyzer
{
    internal class ListenAndAct
    {
        #region Fields

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static TcpConnectionManager tcpConnectionManager = new TcpConnectionManager();
        private static ICaptureDevice device;

        /// <summary>
        /// data to be stored in file
        /// </summary>
        private static string data;

        #endregion

        internal static void StartListening()
        {
            try
            {
                // Print SharpPcap version
                string ver = SharpPcap.Version.VersionString;
                Console.WriteLine("SharpPcap {0}", ver);

                // Retrieve the device list
                var devices = SharpPcap.LibPcap.LibPcapLiveDeviceList.Instance;

                // If no devices were found print an error
                if (devices.Count < 1)
                {
                    Console.WriteLine("No devices were found on this machine");
                    return;
                }

                Console.WriteLine();
                Console.WriteLine("The following devices are available on this machine:");
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine();

                int i = 0;

                // Print out the devices
                foreach (SharpPcap.LibPcap.LibPcapLiveDevice dev in devices)
                {
                    /* Description */
                    Console.WriteLine("{0}) {1} {2} {3}", i, dev.Name, dev.Description, dev.Interface.Description);
                    i++;
                }

                ////autochose loopback
                //var loopback = devices.Where(d => d.Name.Contains("Loopback")).First();
                //device = loopback;

                //autochose loopback
                var outgoingInterface = devices.Where(d => d.Description.Contains("Realtek PCIe")).First();
                device = outgoingInterface;

                Console.WriteLine();
                //Console.Write("-- Please choose a device to capture: ");
                //i = int.Parse(Console.ReadLine());

                //ICaptureDevice device = devices[i];

                tcpConnectionManager.OnConnectionFound += HandleTcpConnectionManagerOnConnectionFound;

                // Register our handler function to the 'packet arrival' event
                device.OnPacketArrival +=
                    new PacketArrivalEventHandler(device_OnPacketArrival);

                // Open the device for capturing
                int readTimeoutMilliseconds = 1000;
                device.Open(DeviceMode.Promiscuous, readTimeoutMilliseconds);

                Console.WriteLine();
                Console.WriteLine("-- Listening on {0} {1}, hit 'Enter' to stop...",
                    device.Name, device.Description);

                // Start the capturing process
                device.StartCapture();

            }
            catch (Exception exc)
            {
                Console.WriteLine();
                Console.WriteLine($"{exc.Message} + { (exc.InnerException != null ? exc.InnerException.Message : string.Empty) } ");
            }

        }

        internal static void StopListening()
        {
            //// Wait for 'Enter' from the user.
            //Console.ReadLine();

            if (device != null)
            {
                // Stop the capturing process
                device.StopCapture();
            }

            Console.WriteLine("-- Capture stopped.");

        }



        static void HandleTcpConnectionManagerOnConnectionFound(TcpConnection c)
        {
            try
            {
                //Console.WriteLine("Connection found.");
                var httpSessionWatcher = new HttpSessionWatcher(c,
                                                                OnHttpRequestFound,
                                                                OnHttpStatusFound,
                                                                OnHttpWatcherError);
            }
            catch (Exception exc)
            {
                Console.WriteLine();
                Console.WriteLine($"{exc.Message} + { (exc.InnerException != null ? exc.InnerException.Message : string.Empty) } ");
            }
        }

        /// <summary>
        /// Prints the time and length of each received packet
        /// </summary>
        private static void device_OnPacketArrival(object sender, CaptureEventArgs e)
        {
#if false
            var time = e.Packet.Timeval.Date;
            var len = e.Packet.Data.Length;
            Console.WriteLine("{0}:{1}:{2},{3} Len={4}",
                time.Hour, time.Minute, time.Second, time.Millisecond, len);
            Console.WriteLine(e.Packet.ToString());
#endif
            var p = PacketDotNet.Packet.ParsePacket(e.Packet.LinkLayerType, e.Packet.Data);
            var tcpPacket = p.Extract<TcpPacket>();

            if (tcpPacket == null)
                return;

            log.Debug("passing packet to TcpConnectionManager");
            tcpConnectionManager.ProcessPacket(e.Packet.Timeval,
                                               tcpPacket);
        }

        private static void OnHttpRequestFound(HttpSessionWatcherRequestEventArgs e)
        {
            try
            {
                Console.WriteLine("Http Request captured!");

                log.Debug("");

                //// only display compressed messages
                //if ((e.Request.ContentEncoding == HttpMessage.ContentEncodings.Deflate) ||
                //   (e.Request.ContentEncoding == HttpMessage.ContentEncodings.Gzip))
                //{
                //    //               log.Info(e.Request.ToString());
                //    Console.WriteLine(e.Request.ToString());
                //}

                var filePath = $@"{Directory.GetCurrentDirectory()}\StoredRequests.txt";
                FileStream stream;
                
                //create file if it doesn't exist
                if (File.Exists(filePath))
                {
                    stream = new FileStream(filePath, FileMode.Append);
                }
                else
                {
                    stream = new FileStream(filePath, FileMode.Create);
                }

                using (StreamWriter writer = new StreamWriter(stream))
                {
                    var sensitive = Helper.ProcessOutputData(out data, e.Request);

                    if (sensitive)
                    {
                        writer.WriteLine(data);
                        writer.WriteLine();
                        Console.WriteLine($"Sensitive info written to {stream.Name}.");
                    }
                    else
                    {
                        Console.WriteLine("No form data found");
                    }
                }


                // NOTE: Regex on the url can be performed here on
                // e.Request.Url
            }
            catch (Exception exc)
            {
                Console.WriteLine();
                Console.WriteLine("Exception on 'Request found':");
                Console.WriteLine($"{exc.Message} + { (exc.InnerException != null ? exc.InnerException.Message : string.Empty) } ");
            }
        }

        private static void OnHttpStatusFound(HttpSessionWatcherStatusEventArgs e)
        {
            log.Debug("");

            // only display compressed messages
            //if ((e.Status.ContentEncoding == HttpMessage.ContentEncodings.Deflate) ||
            //   (e.Status.ContentEncoding == HttpMessage.ContentEncodings.Gzip))
            //{
            //    //                log.Info(e.Status.ToString());
            //    Console.WriteLine(e.Status.ToString());
            //}
        }

        private static void OnHttpWatcherError(string errorString)
        {
            log.Error("errorString " + errorString);
        }
    }
}
