﻿using RequestAnalyzer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf.Logging;

namespace ConsoleApp1
{
    public class ReqAnalyserService
    {
        #region Fields
        private readonly LogWriter _log = HostLogger.Get<ReqAnalyserService>();
        #endregion

        #region Methods
        public bool Start()
        {
            _log.InfoFormat("Starting to listen");

            //                //attach debugger on deployment
            //#if DEBUG
            //                System.Diagnostics.Debugger.Launch();
            //#endif

            ListenAndAct.StartListening();

            return true;
        }

        public bool Stop()
        {
            _log.InfoFormat("Stopped listening");

            ListenAndAct.StopListening();

            return true;
        }
        #endregion

    }
}

